const baseUrl = "http://localhost:8080/java-ERS-system";
let currID = sessionStorage.getItem("empID");
const pendingUrl = baseUrl + "/pending?empID=" + currID;
let pendingRequests;
var new_tbody = document.createElement('tbody');
var old_tbody = document.getElementById('myTBody');

window.onload = function(){
    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", pendingUrl);
    xhr1.onreadystatechange = function(){
        if(xhr1.readyState==4){
			pendingRequests = JSON.parse(xhr1.response);
			for (let r of pendingRequests) {
			let newRow = document.createElement("tr");
			let IDColumn = document.createElement("td");
  			IDColumn.innerText = r.id;
  			let requestAmount = document.createElement("td");
  			requestAmount.innerText = r.amount.toFixed(2);
			newRow.append(IDColumn,requestAmount);
			new_tbody.appendChild(newRow);	
			}
		old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
		}
    }
	xhr1.send();
}

signOutBtn.addEventListener("click", (e) => {
	sessionStorage.setItem("empID", "");
})
