const baseUrl = "http://localhost:8080/java-ERS-system";
const allPendingUrl = baseUrl + "/allPending";


var submitForm = document.getElementById("formBox");
var submitButton = document.getElementById("submitBtn");
let allPendingRequests1;
let allPendingRequests2;

window.onload = function(){
	allPendingRequests1;
	var new_tbody = document.createElement('tbody');
	var old_tbody = document.getElementById('myTBody');
    var xhr1 = new XMLHttpRequest();
    xhr1.open("GET", allPendingUrl);
    xhr1.onreadystatechange = function(){
        if(xhr1.readyState==4){
			allPendingRequests1 = JSON.parse(xhr1.response);
			for (var r of allPendingRequests1) {			
			var newRow = document.createElement("tr");		
			var usernameColumn = document.createElement("td");
			var fnameColumn = document.createElement("td");
			var lnameColumn = document.createElement("td");
			var IDColumn = document.createElement("td");
  			var requestAmount = document.createElement("td");
			var statusColumn = document.createElement("td");
			usernameColumn.innerText = r.username;
			fnameColumn.innerText = r.fname;
			lnameColumn.innerText = r.lname;
			IDColumn.innerText = r.requestID;
			statusColumn.innerText = r.requestStatus;
  			requestAmount.innerText = r.amount.toFixed(2);
			newRow.append(usernameColumn,fnameColumn,lnameColumn,IDColumn,requestAmount,statusColumn);
			new_tbody.appendChild(newRow);	
			}
		old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
		}
    }
	xhr1.send();
}



submitButton.addEventListener("click", (e) => {
    e.preventDefault();
    var radios = document.getElementsByName('inlineRadioOptions');
	var status;
	for (var i = 0, length = radios.length; i < length; i++) {
    	if (radios[i].checked) {
       		status =radios[i].value
        	break;
    	}
	}
	let requestID = submitForm.requestID.value;
    var userObj = {id: requestID, requestStatus: status};
    var myJSON = JSON.stringify(userObj);
	var xhr1 = new XMLHttpRequest();
    xhr1.open("POST", allPendingUrl);
    xhr1.onreadystatechange = function(){
        if(xhr1.status == 200)
			window.location.reload();
    }
xhr1.send(myJSON);
})
