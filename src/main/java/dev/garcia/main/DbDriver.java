package dev.garcia.main;

import java.sql.Connection;
import java.sql.SQLException;

import dev.garcia.util.ConnectionUtil;

public class DbDriver {
public static void main(String[] args) {
		
		try {
			Connection connection = ConnectionUtil.getConnection();
			System.out.println(connection.getMetaData().getDriverName());
		} catch (SQLException e) {
			System.out.println("Something went wrong:(");
			e.printStackTrace();
		}
		
		
	}
}
