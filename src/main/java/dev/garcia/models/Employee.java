package dev.garcia.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Employee {

	private String username;
	private String password;
	private String fname;
	private String lname;
	private int empID;
	
	public Employee(){
		super();
	}
	
	public Employee(String username, String password, String fname, String lname, int empID){
		super();
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.empID = empID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	
	@Override
	public String toString() {
		return "Employee [username=" + username + ", password=" + password + ", fname=" + fname + ", lname=" + lname
				+ ", empID=" + empID + "]";
	}
}
