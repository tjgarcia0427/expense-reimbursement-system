package dev.garcia.daos;

import java.util.ArrayList;

import dev.garcia.models.Employee;
import dev.garcia.models.detailedRequest;

public interface ManagerDao {
	public ArrayList<detailedRequest> allPendingView();
	public void updateStatusResolved(int requestID);
	public void updateStatusDenied(int requestID);
	public ArrayList<detailedRequest> allResolvedView();
	//public ArrayList<Employee> employeeView();	
}
