package dev.garcia.servlets;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.garcia.models.Request;
import dev.garcia.services.EmployeeService;


public class SubmitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EmployeeService employeeService = new EmployeeService();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		BufferedReader bw = request.getReader();
		String JSON = "";
		String line = bw.readLine();
		while(line != null) {
			JSON = JSON + line;
			line = bw.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Request r = om.readValue(JSON, Request.class);

		double amount = r.getAmount();
		int empID = r.getId();
		employeeService.insertRequest(empID, amount);
		response.setStatus(200);
	}
}
