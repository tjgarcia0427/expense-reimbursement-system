package dev.garcia.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.garcia.models.Request;
import dev.garcia.services.EmployeeService;

public class ResolvedRequestsServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private EmployeeService employeeService = new EmployeeService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ArrayList<Request> resolvedList = employeeService.resolvedView(Integer.valueOf(request.getParameter("empID")));
		ObjectMapper om = new ObjectMapper();
		String requestsJson = om.writeValueAsString(resolvedList);
		PrintWriter pw = response.getWriter();
		pw.write(requestsJson);
		pw.close();
	}
}
