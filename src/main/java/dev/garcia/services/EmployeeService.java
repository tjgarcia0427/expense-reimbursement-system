package dev.garcia.services;

import java.util.ArrayList;

import dev.garcia.daos.EmployeeDao;
import dev.garcia.daos.EmployeeDaoImpl;
import dev.garcia.models.Employee;
import dev.garcia.models.Request;

public class EmployeeService {
	private EmployeeDao employeeDao = new EmployeeDaoImpl();
	
	public void insertRequest(int requestID, double amount) {
		employeeDao.insertRequest(requestID, amount);
	}
	public ArrayList<Request> pendingView(int requestID){
		return employeeDao.pendingView(requestID);
	}
	public ArrayList<Request> resolvedView(int requestID){
		return employeeDao.resolvedView(requestID);
	}
	public Employee employeeProfileView(String username) {
		return employeeDao.employeeProfileView(username);
	}
	/*
	public void updateProfile(String username, String password, String fname, String lname) {
		employeeDao.updateProfile(username, password, fname, lname);
	}
	*/
}
