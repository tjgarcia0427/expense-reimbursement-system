package dev.garcia.services;

import org.apache.juli.logging.Log;

import dev.garcia.daos.UserDao;
import dev.garcia.daos.UserDaoImpl;

public class UserService {
	private UserDao userDao = new UserDaoImpl();
	
	public int isEmployee(String username, String password) {
		return userDao.isEmployee(username, password);
	}
	
	public boolean isManager(String username, String password) {
		return userDao.isManager(username, password);
	}
}
