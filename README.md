# Expense Reimbursement System

## Project Description

This project was a full stack application utilizing HTML, CSS, and JavaScript on the frontend and a Java application on the backend. This Java application utilized Servlets to process user requests from the front end. 

## Technologies Used

* Java - version 1.8
* JavaScript
* Servlets
* JDBC
* SQL
* HTML
* CSS
* Bootstrap
* AJAX
* JUnit
* Log4j

## Features

* An Employee can login
* An Employee can view the Employee Homepage
* An Employee can logout
* An Employee can submit a reimbursement request
* An Employee can view their pending reimbursement requests
* An Employee can view their resolved reimbursement requests

* A Manager can login
* A Manager can view the Manager Homepage
* A Manager can logout
* A Manager can approve/deny pending reimbursement requests
* A Manager can view all pending requests from all employees

To-do list:
* Improve CSS of website and make it look more presentable

## Getting Started
   
(include git clone command)
(include all environment setup steps)

> Be sure to include BOTH Windows and Unix command  
> Be sure to mention if the commands only work on a specific platform (eg. AWS, GCP)

- All the `code` required to get started
- Images of what it should look like

## Usage

> Here, you instruct other people on how to use your project after they’ve installed it. This would also be a good place to include screenshots of your project in action.

